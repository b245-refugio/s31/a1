const http = require("http");
const port = 3000;
const server = http.createServer((req, res)=>{
	if (req.url == '/login') {
		res.writeHead(200,{'Content-type': 'text/plain'});
		res.end("Welcome to the login page");
	} else {
		res.writeHead(404,{'Content-type': 'text/plain'});
		res.end("Page not found");
	}
});

server.listen(port);

console.log(`Server is successfully running at localhost: ${port}`);